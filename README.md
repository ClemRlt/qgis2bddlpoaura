# Projets de travail pour QGIS du réseau LPO Auvergne-Rhône-Alpes

Les projets sont les fichiers portant l'extension `qgs`.

Ces projets intègrent des actions facilitant l'exploitation des données. Elles sont présentées dans le dossier `action_scripts`:
* `zone_etude.py` : Les actions attribuées à la couche zone d'étude
* `commune.py` : Les actions attribuées à la couche des communes

Le dépot d'origine du projet est sur [Framagit](https://framagit.org/lpoaura/qgis2bddlpoaura/)
